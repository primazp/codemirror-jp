'use strict';

module.exports = function(grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    concat: {
      js: {
        options: {
          separator: ';'
        },
        files: {
          'dist/codemirror-jp.js': [ 'src/js/*.js' ]
        }
      }
    },
    clean: {
      dist: {
        src: ['dist']
      }
    },
    copy: {
      options: {
        processContent: false,
        processContentExclude: []
      },
      dist: {
        files: [
          {expand: true, cwd: 'src/css/', src: ['*.css'], dest: 'dist/'}
        ]
      }
    }
  });

  grunt.registerTask('build', [ 'clean:dist', 'concat:js', 'copy:dist' ]);
};

